<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php
  $user_entity_info = null;
  $uid = $row->field_field_creator[0]['raw']['entity']->uid;
  $user_profile_query = db_select('ostadmajazi_user_profile', 'oup');
  $user_profile_query->fields('oup', ['profile_link']);
  $user_profile_query->condition('uid', $uid);
  $user_profile = $user_profile_query->execute()->fetchAssoc();
  if ($row->field_field_creator) {
    $user_entity_info = $row->field_field_creator[0]['rendered']['#item']['entity'];
  }
?>
<?php if (!empty($user_entity_info)): ?>
<div class="user-profile">
  <a href="<?php print url('autor/' . $user_profile['profile_link'], ['absolute' => TRUE]); ?>">
    <?php
    $image_variables = [
      'path' => $user_entity_info->picture->uri, 
      'alt' => 'User Picture',
      'attributes' => ['class' => ['img-responsive']]
    ];
    print theme_image($image_variables);
    ?>
    <p><?php print $user_entity_info->field_name['und'][0]['safe_value']; ?> <?php print $user_entity_info->field_family['und'][0]['safe_value']; ?></p>
  </a>
</div>
<?php endif; ?>
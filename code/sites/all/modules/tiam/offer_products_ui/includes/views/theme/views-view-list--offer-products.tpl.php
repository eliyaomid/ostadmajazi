<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 *
 * @ingroup views_templates
 */
?>
<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php foreach ($rows as $id => $row): ?>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
      <div class="<?php print $classes_array[$id]; ?>">
        <?php print $row; ?>
      </div>
    </div>
  <?php endforeach; ?>
<?php print $wrapper_suffix; ?>

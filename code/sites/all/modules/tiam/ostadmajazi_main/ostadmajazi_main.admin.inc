<?php

/**
 * Render newsletter list
 */
function newsletter_list() {
  $header = [
    ['data' => 'شناسه', 'field' => 'id'],
    ['data' => 'ایمیل', 'field' => 'mail']
  ];
  $newsletter_list = db_select('ostadmajazi_newsletter', 'onl')
    ->fields('onl', ['id', 'mail'])
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(10)
    ->execute();
  $rows = [];
  foreach($newsletter_list as $newsletter) {
    $rows[] = [
      ['data' => $newsletter->id],
      ['data' => $newsletter->mail]
    ];
  }
  $table_output = theme('table', [
    'header' => $header, 
    'rows' => $rows, 
  ]);
  $page_output = $table_output.theme('pager');
  return $page_output;
}

function collaborate_us_admin_callback() {
  $header = [
    ['data' => 'شناسه', 'field' => 'id'],
    ['data' => 'نام و نام خانوادگی', 'field' => 'name'],
    ['data' => 'ایمیل', 'field' => 'email'],
    ['data' => 'تلفن موبایل', 'field' => 'phone'],
    ['data' => 'عنوان دوره آموزشی', 'field' => 'topic'],
//    ['data' => 'دسته بندی', 'field' => 'category'],
    ['data' => 'لینک', 'field' => 'url'],
    ['data' => 'درباره مدرس', 'field' => 'about_teacher'],
    ['data' => 'توضیح', 'field' => 'description']
    
  ];
  $request_list = db_select('ostadmajazi_collaborate_us', 'ocu')
    ->fields('ocu', ['id', 'name', 'phone', 'email', 'topic', 'category', 'url', 'about_teacher', 'description'])
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(10)
    ->execute();
  $rows = [];
  foreach($request_list as $request) {
//    $category = "";
//    foreach (json_decode($request->category) as $ct) {
//      $category .= " " . $ct . ",";
//    }
//    $category = rtrim($category, ',');
    $rows[] = [
      ['data' => $request->id],
      ['data' => $request->name],
      ['data' => $request->email],
      ['data' => $request->phone],
      ['data' => $request->topic],
//      ['data' => $category],
      ['data' => $request->url],
      ['data' => $request->about_teacher],
      ['data' => $request->description]
    ];
  }
  $table_output = theme('table', [
    'header' => $header, 
    'rows' => $rows, 
  ]);
  $page_output = $table_output.theme('pager');
  return $page_output;
}

function contact_us_admin_callback() {
  $header = [
    ['data' => 'شناسه', 'field' => 'id'],
    ['data' => 'نام و نام خانوادگی', 'field' => 'name'],
    ['data' => 'تلفن تماس', 'field' => 'phone'],
    ['data' => 'ایمیل', 'field' => 'email'],
    ['data' => 'موضوع', 'field' => 'subject'],
    ['data' => 'متن پیام', 'field' => 'message']    
  ];
  $request_list = db_select('ostadmajazi_contact_us', 'ocu')
    ->fields('ocu', ['id', 'name', 'phone', 'email', 'subject', 'message'])
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(10)
    ->execute();
  $rows = [];
  foreach($request_list as $request) {
    $rows[] = [
      ['data' => $request->id],
      ['data' => $request->name],
      ['data' => $request->phone],
      ['data' => $request->email],
      ['data' => $request->subject],
      ['data' => $request->message]
    ];
  }
  $table_output = theme('table', [
    'header' => $header, 
    'rows' => $rows, 
  ]);
  $page_output = $table_output.theme('pager');
  return $page_output;
}
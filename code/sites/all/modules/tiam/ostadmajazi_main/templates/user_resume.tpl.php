<div class="user-info-wrap col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <?php
  $image_variables = [
    'path' => $user->picture->uri,
    'alt' => 'User Picture',
    'attributes' => ['class' => ['img-responsive']]
  ];
  print theme_image($image_variables);
  ?>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
  <h3><?php print $user->field_name['und'][0]['safe_value'];?> <?php print $user->field_family['und'][0]['safe_value'];?> </h3>
  <div class="user-description">
    <?php print $user->field_user_profile_description['und'][0]['safe_value']; ?>
  </div>
  </div>
</div>
<div class="clearfix"></div>
<br>
<hr>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <?php print $user_product; ?>
</div>
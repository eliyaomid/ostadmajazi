<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php if ($row->field_commerce_price[0]['raw']['original']['amount'] != $row->field_commerce_price[0]['raw']['amount']): ?>
<div class="discount">
  <?php
  $original_amount = $row->field_commerce_price[0]['raw']['original']['amount'];
  $amount = $row->field_commerce_price[0]['raw']['amount'];
  $discount =  (100*(int)$amount)/(int)$original_amount;
  print $discount."%";
  ?>
</div>
<div class="original">
  <?php print $row->field_commerce_price[0]['raw']['original']['amount']; ?>
</div>
<div class="clearfix"></div>
<div class="amount">
  <?php print $row->field_commerce_price[0]['raw']['amount']; ?>
</div>
<?php else: ?>
<div class="amount">
  <?php print $row->field_commerce_price[0]['raw']['amount']; ?>
</div>
<?php endif; ?>
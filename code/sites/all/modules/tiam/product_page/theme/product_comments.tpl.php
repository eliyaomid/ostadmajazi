<div class="comments-wrapper">
  <h3>نظرات:</h3>
  <?php foreach ($comments as $comment): ?>
  <div class="comment">
    <h4><?php print $comment->user_name; ?></h4>
    <p><?php print $comment->comment; ?></p>
  </div>
  <?php endforeach; ?>
</div>
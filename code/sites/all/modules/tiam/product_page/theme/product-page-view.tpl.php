<div class="product-page-wrapper col-sm-12">
  <h1 class="product-title"><?php print $element['product_title'];?></h1>
  <div class="product-page-info col-sm-4">
    <div class="info-box">
      <?php print render($element['field_rate']);?>
      <?php if (!empty($element['discount'])): ?>
      <div class="percentage-discount">
        <?php print $element['discount']; ?>%
      </div>
      <?php endif; ?>
      <div class="element-box">
        <div class="info-item creator">
          <div class="creator-image">
            <a href="<?php print url('autor/' . $element['field_creator']['profile_link'], ['absolute' => TRUE]);?>">
              <?php
              $image_variables = [
                'path' => $element['field_creator']['picture_uri'],
                'alt' => 'User Picture',
                'attributes' => ['class' => ['img-responsive']]
              ];
              print theme_image($image_variables);
              ?>
            </a>
          </div>
          <div class="creator-name"><?php print $element['field_creator']['name']; ?></div>
        </div>
        <div class="info-item category">
          <?php print render($element['field_category']);?>
        </div>
        <div class="info-item period">
          <?php print render($element['field_period']);?>
        </div>
        <div class="clearfix"></div>
        
        <div class="info-item ages">
          <?php print render($element['field_ages']);?>
        </div>
        <div class="info-item language">
          <?php print render($element['field_language']);?>        
        </div>
        <div class="info-item price">
          <?php print $element['commerce_price'];?>
        </div>
        <div class="clearfix"></div>
        <div class="info-item add-to-cart">
          <?php print render($element['add_to_cart_form']);?>
        </div>
      </div>
    </div>
    <div class="share-box">
      <p>با دوستان خود به اشتراک بگذارید</p>
      <div class="share-links">
        <?php print $element['share_links'];?>
      </div>
    </div>
  </div>
  <div class="left-coll col-sm-8">
    <div class="product-page-player-holder">
      <?php if (!empty($element['field_trailer'])): ?>
      <?php print render($element['field_trailer']);?>
      <?php else: ?>
      <?php print render($element['field_image']);?>
      <?php endif; ?>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="report-problem">
    <div id="report-problem-modal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">گزارش مشکل اخلاقی</h4>
          </div>
          <div class="modal-body">
            <?php echo render($element['report_problem_form']);?>
          </div>
          <!--<div class="modal-footer">
            
          </div>-->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <a id="report-problem-btn" class="btn btn-success pull-right" data-toggle="modal" data-target="#report-problem-modal">گزارش مشکل اخلاقی</a>
  </div>
  <div class="clearfix"></div>
  <div class="col-sm-12 product-page-description">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#description-title">توضیحات و سرفصل های دوره</a></li>
      <li><a data-toggle="tab" href="#user-comment">نظرات کاربران</a></li>
    </ul>
    <div class="tab-content">
      <div id="description-title" class="tab-pane fade in active">
        <a href="<?php print url('autor/' . $element['field_creator']['profile_link'], ['absolute' => TRUE]);?>" class="btn btn-danger">
          درباره مدرس
        </a>
        <?php print render($element['field_description']);?>
        <?php if (!empty($element['commerce_file'])): ?>
        <br>
        <hr>
        <h4>فایل های دوره آموزشی</h4>
        <hr>
        <?php print render($element['commerce_file']);?>
        <?php endif; ?>
      </div>
      <div id="user-comment" class="tab-pane fade">
        <?php print render($element['comment_form']);?>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="col-sm-12 product-page-tags">
    <div class="title-label">
      <h3>کلمات کلیدی</h3>
    </div>
    <?php print render($element['field_tags']);?>
  </div>
   <div class="clearfix"></div>
  <div class="col-sm-12 product-related">
    <div class="title-label">
      <h3>دوره های آموزشی مشابه</h3>
    </div>
    <?php print render($element['field_product_related']);?>
  </div>
   <div class="col-sm-12 product-related">
    <div class="title-label">
      <h3>سایر دوره های آموزشی این مدرس</h3>
    </div>
    <?php print render($element['field_other_product_author']);?>
  </div>
  <script>
  var productRelatedSwiper = new Swiper ('.swiper-container', {
   slidesPerView: 4,
   nextButton: '.swiper-button-next',
   prevButton: '.swiper-button-prev',
   freeMode: true,
   breakpoints: {
     1024: {
       slidesPerView: 4
     },
     768: {
       slidesPerView: 2
     },
     640: {
       slidesPerView: 1
     },
     320: {
       slidesPerView: 1
     }
   }
  });
  </script>
</div>
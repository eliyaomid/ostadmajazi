<?php

/**
 * Render product page content.
 */
function product_page_view($product) {
  $entity_element = [
    'product_title' => $product->title
  ];
  $view_mode = 'full';
  $entity_type = 'commerce_product';
  $product_id = entity_id($entity_type, $product);

  #Make entity view of product
  field_attach_prepare_view($entity_type, [$product_id => $product], $view_mode);
  entity_prepare_view($entity_type, [$product_id => $product]);
  $entity_element += field_attach_view($entity_type, $product, $view_mode);
  
  #Make breadcrumb Home -> Product category
  $breadcrumb = [
    l('خانه', '<front>')
  ];
  if ($product->field_category && array_key_exists('und', $product->field_category)) {
    $taxonomy_term = $product->field_category['und'][0]['taxonomy_term'];
    $taxonomy_id = $taxonomy_term->tid;
    $all_category = array_reverse(taxonomy_get_parents_all($taxonomy_id));
    foreach ($all_category as $category) {
      array_push($breadcrumb, l($category->name, 'products-list/' . $category->tid));
    }
  }

  drupal_set_breadcrumb($breadcrumb);
  
  #Make add to cart form of product
  $add_to_cart_form_id = commerce_cart_add_to_cart_form_id(array($product->product_id));
  $line_item = commerce_product_line_item_new($product);
  $line_item->data['context']['product_ids'] = array($product->product_id);
  $add_to_cart_form = drupal_get_form($add_to_cart_form_id, $line_item);
  $entity_element['add_to_cart_form'] = $add_to_cart_form;
  
  #Make field creator
  $uid = $entity_element['field_creator'][0]['#item']['entity']->uid;
  $user_profile_query = db_select('ostadmajazi_user_profile', 'oup');
  $user_profile_query->fields('oup', ['profile_link']);
  $user_profile_query->condition('uid', $uid);
  $user_profile = $user_profile_query->execute()->fetchAssoc();
  $user_entity_info = $entity_element['field_creator'][0]['#item']['entity'];
  
  $entity_element['field_creator'] = [
    'name' => $user_entity_info->field_name['und'][0]['safe_value'] . $user_entity_info->field_family['und'][0]['safe_value'],
    'profile_link' => $user_profile['profile_link'],
    'picture_uri' => $user_entity_info->picture->uri
  ];
  
  #Make field price
  $amount = $entity_element['commerce_price']['#items'][0]['amount'];
  $original_amount = $entity_element['commerce_price']['#items'][0]['original']['amount'];
  $entity_element['commerce_price'] = '<span class="amount">'.$amount.' ریال</span>';
  if ($amount != $original_amount) {
    $discount =  (100*(int)$amount)/(int)$original_amount;
    $entity_element['discount'] = $discount;
    $entity_element['commerce_price'] = '<span class="orginal-amount">'.$original_amount.'</span>'.$entity_element['commerce_price'];
  }
  

  #Make field product related
  $product_catalog = views_get_view('product_catalog');
  $product_catalog->set_display('page');
  $pager = $product_catalog->display_handler->get_option('pager');
  $pager['type'] = 'some';
  $pager['options']['items_per_page'] = 8;
  $product_catalog->display_handler->set_option('pager', $pager);
  $product_catalog->pre_execute();
  $product_catalog = $product_catalog->render('page');
  $entity_element['field_product_related'] = $product_catalog;

  #Make field other product of this author
  $product_catalog = views_get_view('product_catalog');
  $product_catalog->set_display('page');
  $pager = $product_catalog->display_handler->get_option('pager');
  $pager['type'] = 'some';
  $pager['options']['items_per_page'] = 8;
  $product_catalog->display_handler->set_option('pager', $pager);
  /* Filter criterion: Commerce Product: Author name (field_creator) */
  $filters = $product_catalog->display_handler->get_option('filters');
  $filters['field_creator_target_id']['id'] = 'field_creator_target_id';
  $filters['field_creator_target_id']['table'] = 'field_data_field_creator';
  $filters['field_creator_target_id']['field'] = 'field_creator_target_id';
  $filters['field_creator_target_id']['value']['value'] = \
          sprintf("%s", $uid);
  $product_catalog->display_handler->set_option('filters', $filters);
  $product_catalog->pre_execute();
  $product_catalog = $product_catalog->render('page');
  $entity_element['field_other_product_author'] = $product_catalog;
  
  #Make commerce files
  if (!empty($entity_element['commerce_file'])) {
    $files_items = "";
    $all_files = $entity_element['commerce_file']['#items'];
    foreach ($all_files as $key => $file_item) {
      $entity_element['commerce_file'][$key]['#theme'] = 'commerce_file_product_page_download_link';
    }
  }

  #Make report problem form
  $report_problem_form = drupal_get_form(
          'report_problem_form',
          $entity_element['product_title'],
          url('products/' . $product->sku, ['absolute' => TRUE])
  );
  $entity_element['report_problem_form'] = $report_problem_form;

  #Make field comment form
  $entity_element['comment_form'] = drupal_get_form('product_comment_form', $product->product_id);
  
  #Make share links
  $entity_element['share_links'] = addtoany_create_buttons();

  #Add Swiper(product slider) script
  $swiper_libraries_path = libraries_get_path('Swiper');

  return [
    '#theme'     => 'product_page_view',
    '#element'   => $entity_element,
    '#attached' => [
      'css' => [
        drupal_get_path('module', 'product_page') . '/css/product_page.css',
        $swiper_libraries_path.'/dist/css/swiper.min.css'
      ],
      'js' => [
        $swiper_libraries_path.'/dist/js/swiper.min.js'
      ]
    ]
  ];
}

function product_comment_form($form, &$form_state, $product_id) {
  $form = [
    '#prefix' => '<div class="col-xs-12 col-sm-8 col-md-5 col-lg-5">',
    '#suffix' => '</div><div class="clearfix"></div>'
  ];

  $comments_query = db_select('ostadmajazi_product_comment', 'opc');
  $comments_query->fields('opc', ['id', 'user_name', 'comment']);
  $comments_query->condition('product_id', $product_id);
  $comments = $comments_query->execute()->fetchAllAssoc('id');
  $form['wrapper'] = [
    '#markup' => theme('product_comments', ['comments' => $comments])
  ];

  $form['product_id'] = [
    '#type' => 'hidden',
    '#default_value' => $product_id
  ];
  $form['user_name'] = [
    '#type' => 'textfield',
    '#title' => 'نام شما',
    '#attributes' => ['placeholder' => 'نام و نام خانوادگی'],
    '#maxlength' => 255
  ];
  $form['email'] = [
    '#type' => 'textfield',
    '#title' => 'ایمیل',
    '#attributes' => ['placeholder' => 'ایمیل'],
    '#maxlength' => 255
  ];
  $form['comment'] = [
    '#type' => 'textarea',
    '#title' => 'نظر',
    '#attributes' => ['placeholder' => 'نظر شما']
  ];
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'ثبت نظر'
  ];
  
  return $form;
}

function product_comment_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  db_insert('ostadmajazi_product_comment')
    ->fields([
      'product_id' => $values['product_id'],
      'user_name' => $values['user_name'],
      'email' => $values['email'],
      'comment' => $values['comment']
    ])->execute();
  drupal_set_message('نظر شما با موفقیت ثبت شد');
}
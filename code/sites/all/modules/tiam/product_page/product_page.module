<?php
/**
 * Implements hook_menu().
 */
function product_page_menu() {
  $items = [];

  $items['products/%commerce_product_sku'] = [
    'title callback' => 'product_page_title',
    'title arguments' => [1],
    'page callback' => 'product_page_view',
    'page arguments' => [1],
    'access arguments' => ['access content'],
    'expanded' => TRUE,
    'weight' => 1,
    'type' => MENU_CALLBACK,
    'file' => 'product_page.pages.inc'
  ];

  $items['admin/product-reports'] = [
    'title' => 'Product Reports',
    'description' => 'Show list of all product reports',
    'page callback' => 'product_reports',
    'access arguments' => ['administer product_reports']
  ];

  return $items;
}

/**
 * Loads product by sku.
 */
function commerce_product_sku_load($sku) {
  $product = commerce_product_load_by_sku(trim($sku));
  return $product;
}

/**
 * Set Product page content.
 */
function product_page_title($product) {
  return $product->title;
}

/**
 * Implements hook_theme().
 */
function product_page_theme() {
  return [
    'product_page_view' => [
      'variables' => ['element' => NULL],
      'template' => 'theme/product-page-view',
    ],
    'product_comments' => [
      'variables' => ['comments' => []],
      'template' => 'theme/product_comments',
    ],
    'commerce_file_product_page_download_link' => [
      'variables' => ['file' => NULL, 'license' => NULL, 'icon' => NULL]
    ]
  ];
}

/**
 * Copy of theme_file_file_link() for linking to the file download URL.
 *
 * @see theme_file_file_link()
 */
function theme_commerce_file_product_page_download_link($variables) {
  $file = $variables['file'];
  if (in_array($file->filemime, ['video/mp4', 'video/x-flv'])) {
    if ($wrapper = file_stream_wrapper_get_instance_by_uri($file->uri)) {
      $file_url = $wrapper->getExternalUrl();
      return '<video controls><source src="'.$file_url.'" type="'.$file->filemime.'"></video>';
    } else {
      return '';
    }
  }
  $uri = array('path' => "file/{$file->fid}/download", 'options' => array());
  $uri['options']['query']['token'] = commerce_file_get_download_token($file);
  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $uri['options']['attributes']['type'] = $file->filemime . '; length=' . $file->filesize;
  // Use the description as link text if found, or fallback to the filename.
  $label = !empty($file->description) ? $file->description : $file->filename;
  // Prepare the icon.
  $icon = ' ';
  if (!empty($variables['icon'])) {
    $icon = $variables['icon'] . ' ';
  }

  if (empty($variables['license'])) {
    $can_download = commerce_file_bypass_license_control();
  }
  else {
    // The file that this theme function receives is just the filefield item
    // array converted to an object (yay, D7!). The limit download check needs
    // the actual file entity.
    $real_file = file_load($file->fid);
    $can_download = commerce_file_can_download($variables['license'], $real_file);
  }

  $output = '<span class="file">' . $icon;
  $output .= $can_download ? l($label, $uri['path'], $uri['options']) : $label;
  $output .= ' </span>';
  return $output;
}

function report_problem_form($form, &$form_state, $product_name, $product_link) {
  $form = [];

  $form['product_link'] = [
    '#type' => 'hidden',
    '#value' => $product_link,
  ];

  $form['product_name'] = [
    '#title' => 'نام آموزش',
    '#type' => 'textfield',
    '#default_value' => $product_name,
    '#attributes' => ['READONLY' => 'READONLY']
  ];

  $form['message'] = [
    '#title' => 'متن گزارش',
    '#type' => 'textarea',
    '#cols' => 20,
    '#required' => FALSE
  ];

  $form['submit'] = [
    '#value' => 'ارسال',
    '#type' => 'submit',
    '#attributes' => ['class' => ['btn', 'btn-primary']]
  ];

  return $form;
}

function report_problem_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  db_insert('ostadmajazi_product_reports')
    ->fields([
      'product_link' => $values['product_link'],
      'product_name' => $values['product_name'],
      'message' => $values['message']
    ])->execute();
  drupal_set_message('گزارش شما با موفقیت ثبت شد.');
}

function product_reports() {
  $header = [
    ['data' => 'شناسه', 'field' => 'id'],
    ['data' => 'نام محصول', 'field' => 'product_name'],
    ['data' => 'متن گزارش', 'field' => 'message']
  ];
  $request_list = db_select('ostadmajazi_product_reports', 'opr')
    ->fields('opr', ['id', 'product_name', 'message', 'product_link'])
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(10)
    ->execute();
  $rows = [];
  foreach($request_list as $request) {
    $rows[] = [
      ['data' => $request->id],
      ['data' => l(
              $request->product_name,
              $request->product_link,
              ['attributes' => ['target' => '_blank']]
      )],
      ['data' => $request->message]
    ];
  }
  $table_output = theme('table', [
    'header' => $header, 
    'rows' => $rows, 
  ]);
  $page_output = $table_output.theme('pager');
  return $page_output;
}